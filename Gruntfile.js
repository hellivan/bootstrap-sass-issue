'use strict';

module.exports = function(grunt) {

    grunt.initConfig({

	pkg: grunt.file.readJSON('package.json'),

	project: {
	    app: ['app'],
	    assets: ['<%= project.app %>/assets'],
	    css: ['<%= project.assets %>/sass/style.scss']
	},

	connect: {
	    server: {
		options: {
		    livereload: true,
		    port: 9002,
		    base: ['app/']
		}
	    }
	},

	sass: {
	    dev: {
		options: {
		    loadPath: [
			'<%= project.app %>/bower_components'
		    ],
		    style: 'expanded',
		    compass: false
		},
		files: {
		    '<%= project.assets %>/css/style.css':'<%= project.css %>'
		}
	    }
	},

	watch: {
	    sass: {
		files: '<%= project.assets %>/sass/{,*/}*.{scss,sass}',
		tasks: ['sass:dev']
	    },
	    grunt: {
		files: 'Gruntfile.js'
	    }
	}
	
    });

    grunt.loadNpmTasks('grunt-contrib-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-connect');

    grunt.registerTask('default', [
	'sass:dev',
	'connect:server',
	'watch'
    ]);

};
